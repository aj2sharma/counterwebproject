package com.qaagility.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

public class CalmulTest {

    @Test
    public void testMul() {
        int result  = new Calcmul().mul();
        assertEquals(18, result);
    }
}
