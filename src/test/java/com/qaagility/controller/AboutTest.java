package com.qaagility.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

public class AboutTest {

    @Test
    public void testDecreseCounterInputZero()  {
        String result  = new About().desc();
        assertEquals("This application was copied from somewhere, sorry but I cannot give the details and the proper copyright notice. Please don't tell the police!", result);
    }
  
}

