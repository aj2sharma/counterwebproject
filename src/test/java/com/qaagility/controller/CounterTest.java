package com.qaagility.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

public class CounterTest {

	@Test
    public void testCalculateInputBZero()  {
		int result = new Counter().calculate(1, 0);
		assertEquals(Integer.MAX_VALUE, result);
	}

	@Test
    public void testCalculateValidInput()  {
		int result = new Counter().calculate(1, 1);
		assertEquals(1, result);
	}

}
	
