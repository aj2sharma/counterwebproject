package com.qaagility.controller;
 
 public class Counter {
 
     public int calculate(int inputA, int inputB) {
         if (inputB == 0) {
             return Integer.MAX_VALUE;
         }  else {
             return inputA / inputB; 
         }
     }
 
 }
